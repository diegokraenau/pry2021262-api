import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entities/index';
import { CreateUserDto } from './dtos/create-user.dto';
import { updateFilename } from 'src/wireframe/helpers';
import { getFileURLFromBlobStorage, uploadFileToBlobStorage } from 'src/utils';
import { response } from 'express';

//TODO: Remove from here
export interface UserFindOne {
  id?: string;
  email?: string;
}

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async getUsers() {
    return await this.userRepository.find();
  }

  async countUsers() {
    return await this.userRepository.count();
  }

  async getOne(id: string, useEntity?: User) {
    const user = await this.userRepository
      .findOne(id)
      .then((u) => (!useEntity ? u : !!u && useEntity.id === u.id ? u : null));

    if (!user)
      throw new NotFoundException('User does not exists or unauthorized');

    return user;
  }

  async findOne(data: UserFindOne) {
    return await this.userRepository
      .createQueryBuilder('user')
      .where(data)
      .addSelect('user.password')
      .getOne();
  }

  async findByEmail(data: UserFindOne) {
    const response = await this.findOne({ email: data.email });
    if (response) return true;
    return false;
  }

  async updateUser(id: string, dto: CreateUserDto) {
    const validation = await this.findOne({ email: dto.email });

    if (validation && validation.id !== id)
      throw new HttpException(
        'Email used for another account',
        HttpStatus.FORBIDDEN,
      );
    return this.userRepository.update(id, dto);
  }

  async createUser(dto: CreateUserDto) {
    const validation = await this.findByEmail(dto);
    if (validation)
      throw new HttpException('Email already registered', HttpStatus.FORBIDDEN);

    const user = this.userRepository.create(dto);
    return await this.userRepository.save(user);
  }

  async updateUserPhoto(id: string, file: Express.Multer.File) {
    const user = await this.getOne(id);
    file.originalname = updateFilename(file.originalname, user.id);
    const uploaded = await uploadFileToBlobStorage(file);
    if (uploaded) {
      const imgUrl = await getFileURLFromBlobStorage(file);
      const userUpdated = await this.updateUser(id, {
        imgUrl,
      } as CreateUserDto);
      return userUpdated ? imgUrl : null;
    }
    return null;
  }
}

//Tes ci/cd v4.0
