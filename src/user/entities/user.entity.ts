import { Project } from 'src/project/entities';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 70 })
  name: string;

  @Column({ name: 'last_name', type: 'varchar', length: 70 })
  lastName: string;

  @Column({ type: 'varchar', length: 100 })
  email: string;

  @Column({ type: 'varchar', length: 20 })
  password: string;

  @Column({
    type: 'varchar',
    default:
      'https://d3ipks40p8ekbx.cloudfront.net/dam/jcr:3a4e5787-d665-4331-bfa2-76dd0c006c1b/user_icon.png',
  })
  imgUrl: string;

  @Column({ name: 'created_at', type: 'date' })
  created_at: Date;

  @Column({
    name: 'user_type',
    type: 'varchar',
    length: 20,
    default: 'CUSTOMER',
  })
  userType: string;

  @OneToMany(() => Project, (project) => project.user)
  projects: Project[];
}
