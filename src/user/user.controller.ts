import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Put,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/common/decorators';
import { ProjectService } from 'src/project/project.service';
import { UserService } from './user.service';
import { CreateUserDto } from './dtos/create-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';

export interface UserFindOne {
  email?: string;
}

@ApiTags('Users')
@Controller('users')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly projectService: ProjectService,
  ) {}

  // @Auth()
  // @Get()
  // async getUsers() {
  //   const data = await this.userService.getUsers();
  //   return { data };
  // }

  @Auth()
  @Get(':id')
  async getOne(@Param('id') id: string) {
    const data = await this.userService.getOne(id);
    return data;
  }

  @Auth()
  @Get(':id/projects')
  async getProjectsByUserId(@Param('id') id: string) {
    if (await this.userService.getOne(id)) {
      const data = await this.projectService.getProjectsByUserId(id);
      return data;
    } else {
      return null;
    }
  }

  @Post()
  async createUser(@Body() dto: CreateUserDto) {
    const data = await this.userService.createUser(dto);
    return data;
  }

  @Put(':id')
  async updateUser(@Param('id') id: string, @Body() dto: CreateUserDto) {
    await this.userService.updateUser(id, dto);
    const data = this.userService.getOne(id);
    return data;
  }

  @Auth()
  @Put(':id/update-photo')
  @UseInterceptors(FileInterceptor('file'))
  async updateUserPhoto(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    const data = await this.userService.updateUserPhoto(id, file);
    return { url: data };
  }

  @Post('validate')
  async isRegistered(@Body() dto: UserFindOne) {
    const data = this.userService.findByEmail(dto);
    return data;
  }
}
