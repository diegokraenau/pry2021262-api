import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Framework } from './entities/framework.entity';
import { Dependency } from './entities/dependency.entity';
import { DependencyController } from './dependency.controller';
import { DependencyService } from './dependency.service';

@Module({
  imports: [TypeOrmModule.forFeature([Framework, Dependency])],
  controllers: [DependencyController],
  providers: [DependencyService],
  exports: [DependencyService],
})
export class DependencyModule {}
