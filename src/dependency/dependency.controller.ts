import { Controller, Get } from '@nestjs/common';
import { DependencyService } from './dependency.service';

@Controller('dependencies')
export class DependencyController {
  constructor(private readonly dependencyService: DependencyService) {}

  @Get()
  async getFrameworksAndDependencies() {
    const data = await this.dependencyService.getFrameworkAndDependencies();
    return data;
  }
}
