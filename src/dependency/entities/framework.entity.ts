import { PrimaryColumn, Entity, Column, OneToMany } from 'typeorm';
import { Dependency } from './dependency.entity';

@Entity('frameworks')
export class Framework {
  @PrimaryColumn()
  id: number;

  @Column({ type: 'varchar', length: 20 })
  framework: string;

  @OneToMany(() => Dependency, (dependency) => dependency.framework)
  dependencies: Dependency[];
}
