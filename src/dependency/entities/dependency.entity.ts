import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Framework } from './framework.entity';

@Entity('dependencies')
export class Dependency {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 50 })
  name: string;

  @Column({ type: 'varchar', length: 10 })
  version: string;

  @ManyToOne(() => Framework, (framework) => framework.dependencies)
  @JoinColumn({ name: 'framework_id' })
  framework: Framework;
}
