import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Framework } from './entities/framework.entity';
import { Repository } from 'typeorm';
import { Dependency } from './entities/dependency.entity';

@Injectable()
export class DependencyService {
  constructor(
    @InjectRepository(Framework)
    private readonly frameworkRepository: Repository<Framework>,
    @InjectRepository(Dependency)
    private readonly dependencyRepository: Repository<Dependency>,
  ) {}

  async getFrameworkAndDependencies() {
    const frameworks = await this.frameworkRepository.find({
      relations: ['dependencies'],
    });

    return frameworks;
  }

  async getDependencyById(id: string) {
    const dependency = await this.dependencyRepository.findOne({ id });

    return dependency;
  }
}
