import { Module, forwardRef } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Wireframe } from './entities/index';
import { WireframeController } from './wireframe.controller';
import { WireframeService } from './wireframe.service';
import { ProjectModule } from '../project/project.module';
import { HttpModule } from '@nestjs/axios';
import { ViewsModule } from '../views/views.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Wireframe]),
    forwardRef(() => ProjectModule),
    forwardRef(() => ViewsModule),
    HttpModule,
  ],
  exports: [WireframeService],
  controllers: [WireframeController],
  providers: [WireframeService],
})
export class WireframeModule {}
