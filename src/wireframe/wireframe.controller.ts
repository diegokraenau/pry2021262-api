import {
  Body,
  Controller,
  Post,
  UploadedFile,
  UploadedFiles,
  UseInterceptors,
} from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { ApiConsumes, ApiTags } from '@nestjs/swagger';
import { Auth } from 'src/common/decorators';
import { WireframeService } from './wireframe.service';

@ApiTags('Wireframes')
@Controller('wireframes')
export class WireframeController {
  constructor(private readonly wireframeService: WireframeService) {}

  //TODO:Add form-data fields in swagger
  @Auth()
  @Post('generate-GUI')
  @UseInterceptors(AnyFilesInterceptor())
  @ApiConsumes('multipart/form-data')
  async generateGUI(@UploadedFiles() files, @Body() body) {
    const resp = await this.wireframeService.generateGUIfromFilesArray(
      files,
      body,
    );
    return resp;
  }
}
