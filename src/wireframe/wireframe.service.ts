import { Injectable } from '@nestjs/common';
import { uploadFileToBlobStorage } from 'src/utils';
import {
  deleteFilesBlobStorage,
  getFileURLFromBlobStorage,
} from '../utils/app.azure';
import { GenerateGUIDto, GenerateGUIfromFilesArray } from './dto';
import { ProjectService } from '../project/project.service';
import {
  updateFilename,
  filterPredictions,
  skechtHTML,
} from './helpers/wireframe.helper';
import * as probe from 'probe-image-size';
import { HttpService } from '@nestjs/axios';
import { firstValueFrom } from 'rxjs';
import { Prediction } from './entities';
import { Wireframe } from './entities/wireframe.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ViewsService } from '../views/views.service';
import { View } from 'src/views/entities';
import * as moment from 'moment';
import { Project } from '../project/entities/project.entity';

@Injectable()
export class WireframeService {
  constructor(
    @InjectRepository(Wireframe)
    private readonly wireframeRepository: Repository<Wireframe>,
    private readonly projectService: ProjectService,
    private readonly viewService: ViewsService,
    private http: HttpService,
  ) {}

  private predict(Url: string) {
    return this.http.post(
      process.env.API_URL,
      { Url: Url },
      {
        headers: {
          'Prediction-Key': process.env.PREDICTION_KEY,
          'Content-Type': 'application/json',
        },
      },
    );
  }

  async getWireframes() {
    return await this.wireframeRepository.count();
  }

  async createWireframe(entity: Wireframe) {
    return await this.wireframeRepository.save(entity);
  }

  /*Only for 1 file */
  async generateGUI(
    project: Project,
    file: Express.Multer.File,
    generateGUIdto: GenerateGUIDto,
  ) {
    if (project) {
      //Save image on the blob storage
      file.originalname = updateFilename(file.originalname, project.id);
      const uploaded = await uploadFileToBlobStorage(file);
      if (uploaded) {
        const imageURL = await getFileURLFromBlobStorage(file);
        const img = await probe(imageURL);
        console.log('Image size: ', img.width, ' ', img.height);

        //Get the prediction with filters
        const { data } = await firstValueFrom(this.predict(imageURL));
        const predictions: Prediction[] = filterPredictions(data.predictions);

        //Make template
        let [template, content] = skechtHTML(predictions, img);
        template = Buffer.from(template).toString('base64');
        content = Buffer.from(content).toString('base64');
        // contentReact = Buffer.from(contentReact).toString('base64');
        //Save wireframe
        let wireframe: Wireframe = new Wireframe();
        wireframe.image_url = imageURL;
        wireframe.width = img.width;
        wireframe.height = img.height;
        wireframe = await this.createWireframe(wireframe);

        //Save View
        const view: View = new View();
        view.wireframe = wireframe;
        view.content = content;
        // view.contentReact = contentReact;
        view.template = template;
        view.name = generateGUIdto.viewName;
        view.created_date = moment().toDate();
        view.project = project;
        await this.viewService.createOne(view);

        return {
          viewName: generateGUIdto.viewName,
          template,
          predictions,
        };
      } else {
        return { message: 'Failed to upload' };
      }
    }
  }

  async generateGUIfromFilesArray(files: any, generateGUIdto: any) {
    const arrTemplates = [];
    //Parse json objest
    generateGUIdto.views = JSON.parse(generateGUIdto.views);

    //Validate if the project exists
    const project = await this.projectService.findProjectById(
      generateGUIdto.projectId,
    );

    //Iterate wireframes
    for (const view of generateGUIdto.views) {
      let fileFound = null;
      //Find the file
      files.forEach((x) => {
        if (x.originalname == view.imgName) {
          fileFound = x;
        }
      });
      //Save the file and generate gui
      const template = await this.generateGUI(project, fileFound, view);
      arrTemplates.push(template);
    }

    return arrTemplates;
  }
}
