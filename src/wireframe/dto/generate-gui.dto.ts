import { IsNotEmpty, IsString } from 'class-validator';

export class GenerateGUIDto {
  @IsString()
  @IsNotEmpty()
  projectId: string;

  @IsString()
  @IsNotEmpty()
  viewName: string;
}

export class GenerateGUIfromFilesArray {
  // @IsString()
  // @IsNotEmpty()
  projectId: string;

  // @IsNotEmpty()
  // views: any[];
}
