import * as path from 'path';
import { Prediction, Tag } from '../entities';
import * as probe from 'probe-image-size';
import { threshold } from 'src/utils';

//Functions
export const updateFilename = (fileName: string, projectId: string) => {
  const newName = path.parse(fileName).name + '_' + projectId;
  return newName + path.parse(fileName).ext;
};

export const filterPredictions = (predictions: Prediction[]) => {
  return predictions.filter((x) => x.probability * 100 > threshold);
};

const originalHeight = (img: probe.ProbeResult, value: number) => {
  return (value * 100) / img.height; //vh
};

const originalX = (img: probe.ProbeResult, value: number) => {
  return (value * 100) / img.width; //%
};

const originalY = (img: probe.ProbeResult, value: number) => {
  return (value * 100) / img.height;
};

const getVariables = (prediction: Prediction, img: probe.ProbeResult) => {
  let x = prediction.boundingBox.left * img.width; //X in pix
  let y = prediction.boundingBox.top * img.height; //Y in pix
  const width = prediction.boundingBox.width * img.width; //Width in pix
  let height = prediction.boundingBox.height * img.height; //Height in pix
  // console.log(prediction.tagName, x, y, width, height);
  //Refresh items
  // width = originalWidth(img, width);
  height = originalHeight(img, height);
  x = originalX(img, x);
  y = originalY(img, y);

  // console.log(
  //   'Element: ',
  //   prediction.tagName,
  //   'Width: ',
  //   width,
  //   'Height: ',
  //   height,
  //   'X: ',
  //   x,
  //   'Y: ',
  //   y,
  // );

  return [x, y, width, height, img.width, img.height];
};

//Simple HTML
const getContent = (predictions: Prediction[], img: probe.ProbeResult) => {
  // console.log('[predictionsv1] ', predictions);
  predictions.sort((a: Prediction, b: Prediction): number => {
    if (a.boundingBox.top < b.boundingBox.top) return -1;
    if (a.boundingBox.top > b.boundingBox.top) return 1;
    return 0;
  });

  predictions.map((prediction, index) => {
    prediction.index = index;
    prediction.asignedY = false;
    prediction.asignedX = false;
  });

  const foundExectionImage = predictions.filter((x) => x.tagName !== Tag.IMAGE);
  const foundExceptionImageCircle = predictions.filter(
    (x) => x.tagName !== Tag.CIRCLE_IMAGE,
  );
  const images: any = predictions.filter((x) => x.tagName === Tag.IMAGE);
  const imagesCircle: any = predictions.filter(
    (x) => x.tagName === Tag.CIRCLE_IMAGE,
  );

  predictions = [
    ...foundExectionImage,
    ...foundExceptionImageCircle,
    ...images,
    ...imagesCircle,
  ];

  for (let i = 0; i < predictions.length; i++) {
    if (
      predictions[i].asignedY === false &&
      predictions[i].tagName !== Tag.NAV_BAR
    ) {
      const yInicio = predictions[i].boundingBox.top;
      const yFinal =
        predictions[i].boundingBox.top + predictions[i].boundingBox.height;

      const arrayDuplicated = [...predictions];

      for (let j = 0; j < arrayDuplicated.length; j++) {
        if (arrayDuplicated[j].asignedY === false) {
          const y = arrayDuplicated[j].boundingBox.top;
          const yHeight =
            arrayDuplicated[j].boundingBox.top +
            arrayDuplicated[j].boundingBox.height;
          if (
            (yInicio <= y && y <= yFinal) ||
            (yInicio <= yHeight && yHeight <= yFinal)
          ) {
            predictions[j].boundingBox.top = yInicio;
            predictions[j].asignedY = true;
          }
        }
      }
    }
  }

  console.log('[predictionsv3] ', predictions);

  predictions.map((prediction: Prediction) => {
    if (
      prediction.tagName !== Tag.NAV_BAR &&
      prediction.tagName !== Tag.SLIDER
    ) {
      const xInicio = prediction.boundingBox.left;
      const xFinal = prediction.boundingBox.left + prediction.boundingBox.width;

      for (const prediction of predictions) {
        const x = prediction.boundingBox.left;
        const xWidth =
          prediction.boundingBox.left + prediction.boundingBox.width;
        const medio = (x + xWidth) / 2;
        if (
          (xInicio <= x && x <= xFinal) ||
          (xInicio <= xWidth && xWidth <= xFinal) ||
          (xInicio <= medio && medio <= xFinal)
        ) {
          prediction.boundingBox.left = xInicio;
          prediction.asignedX = true;
        }
      }
    }
  });

  // Algoritmo X

  // for (let i = 0; i < predictions.length; i++) {
  //   if(predictions[i].asignedX === false && predictions[i].tagName !== Tag.NAV_BAR && predictions[i].tagName !== Tag.SLIDER){
  //     const xInicio = predictions[i].boundingBox.left;
  //     const xFinal = predictions[i].boundingBox.left + predictions[i].boundingBox.width;

  //     let arrayDuplicated = predictions;

  //     for (let j = 0; j < arrayDuplicated.length; j++) {
  //       if(arrayDuplicated[j].asignedX === false && arrayDuplicated[j].index !== predictions[i].index){
  //         const x = arrayDuplicated[j].boundingBox.left;
  //         const xWidth = arrayDuplicated[j].boundingBox.left + arrayDuplicated[j].boundingBox.width;
  //         const medio = (x + xWidth) / 2;
  //         if ((xInicio <= x && x <= xFinal) || (xInicio <= xWidth && xWidth <= xFinal) || (xInicio <= medio && medio <= xFinal)) {
  //           predictions[j].boundingBox.left = xInicio;
  //           predictions[j].asignedX = true;
  //           console.log('xTrue: ',predictions[j]);
  //         }
  //       }
  //     }
  //   }
  // }

  // separa compoentes

  predictions.map((prediction: Prediction) => {
    const left = prediction.boundingBox.left;
    const top = prediction.boundingBox.top;

    let predictionOthers = predictions.filter(
      (x) => x.index !== prediction.index,
    );
    predictionOthers = predictionOthers.filter(
      (x) => x.tagName !== prediction.tagName,
    );

    for (const predictionOther of predictionOthers) {
      if (
        predictionOther.tagName !== Tag.CHECK_BOX_ON &&
        predictionOther.tagName !== Tag.CHECK_BOX_OFF &&
        predictionOther.tagName !== Tag.RADIO_BUTTON_ON &&
        predictionOther.tagName !== Tag.RADIO_BUTTON_OFF
      ) {
        const xLeft = predictionOther.boundingBox.left;
        const xTop = predictionOther.boundingBox.top;
        if (left === xLeft && top === xTop) {
          predictionOther.boundingBox.left = xLeft + 0.04;
        }
      }
    }
  });

  // console.log('[predictionsv4] ', predictions);

  return `
            ${
              predictions.find((x) => x.tagName == 'NAV_BAR')
                ? draw(
                    predictions.find((x) => x.tagName == 'NAV_BAR'),
                    img,
                    111,
                  )
                : '<div></div>'
            }
            <div class="container-fluid position-relative fill">
            ${predictions
              .filter((x) => x.tagName !== 'NAV_BAR')
              .map((prediction) => draw(prediction, img, prediction.index))
              .join(' ')}  
            </div>
        `;
};

const draw = (
  prediction: Prediction,
  img: probe.ProbeResult,
  index: number,
) => {
  const [x, y, width, height, imgWidth, imgHeight] = getVariables(
    prediction,
    img,
  );
  console.log({ x });
  console.log({ y });
  console.log({ width });
  console.log({ height });
  console.log({ imgWidth });
  console.log({ imgHeight });
  console.log({ index });

  const oneCol = Math.round(imgWidth / 6);
  const oneRow = Math.round(imgHeight / 6);

  const num = 10;
  const numTop = 6;

  // const topNumber = Math.floor(Math.round(y)/numTop)*numTop; // em=16px
  const topNumber = Math.round(y); // em=16px
  // const topNumber = (Math.round(y)/numTop)*numTop; // em=16px
  // const leftNumber = Math.round(Math.round(x)/num)*num;
  const leftNumber = Math.round(x);
  let roundedHeight = Math.round(
    (Math.floor(Math.round(height) / num) * num) / oneRow,
  );
  const colNumber = Math.round(
    (Math.floor(Math.round(width) / num) * num) / oneCol,
  );

  switch (prediction.tagName) {
    case Tag.IMAGE:
      // let size = 0;
      // (colNumber > roundedHeight) ? (size = colNumber) : (size = roundedHeight);
      const size = 1;
      return `
        <div class="square-${size}  mtop-${topNumber} mleft-${leftNumber}">
          <img
            src='https://wireframes.blob.core.windows.net/static/fake_img.svg'
            class='img-fluid image-fixed'
            alt='Responsive image'
          />
        </div>
      `;
    case Tag.INPUT:
      return `
        <div class="width-${colNumber} mtop-${topNumber} mleft-${leftNumber} ${index} ">
          <input
            type="text"
            class="form-control"
            placeholder="Input"
            aria-label="Input"
            aria-describedby="basic-addon1"
          />
        </div>
      `;
    case Tag.BUTTON:
      return `
        <button class="btn width-${colNumber} mtop-${topNumber} mleft-${leftNumber} btn-primary">Button</button>
        `;
    case Tag.LABEL:
      return `
      <div class="width-${colNumber} mtop-${topNumber} mleft-${leftNumber} ${index}">
          <h5>Subtitle</h5>
        </div>
      `;
    case Tag.CARD:
      // roundedHeight = colNumber;
      const sizeImageCard = 1;
      // (colNumber > roundedHeight) ? (sizeImageCard = colNumber) : (sizeImageCard = roundedHeight);
      return `
        <div class="card square-${sizeImageCard} mtop-${topNumber} mleft-${leftNumber} height-${roundedHeight} index-${index}">
          <img
            class="card-img-top cover-img"
            src="https://wireframes.blob.core.windows.net/static/fake_img.svg"
            alt="Card image cap"
          />
          <div class="card-body">
            <p class="card-text">
              Put your card description here.
            </p>
          </div>
        </div>
      `;
    case Tag.NAV_BAR:
      return `
        <nav class="navbar navbar-dark bg-primary navbar-expand-lg position-absolute w-100">
          <a class="navbar-brand" href="#">YourLogo</a>
          <button
            class="navbar-toggler mt-1"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i class="fa fa-bars"></i>
          </button>
          <div class="navbar-collapse collapse" id="navbarNav">
            <ul class="navbar-nav">
              <li class="nav-item py-0">
                <a href="#" class="nav-link">Option 1</a>
              </li>
              <li class="nav-item py-0">
                <a href="#" class="nav-link">Option 2</a>
              </li>
              <li class="nav-item py-0">
                <a href="#" class="nav-link">Option 3</a>
              </li>
              <li class="nav-item py-0">
                <a href="#" class="nav-link">Option 4</a>
              </li>
              <li class="nav-item py-0">
                <a href="#" class="nav-link">Option 5</a>
              </li>
            </ul>
          </div>
        </nav>
      `;
    case Tag.CIRCLE_IMAGE:
      roundedHeight = colNumber;
      return `
        <img
          class="circle-${colNumber} cover-img circle mtop-${topNumber} mleft-${leftNumber}"
          style="border-radius: 50%"
          src="https://wireframes.blob.core.windows.net/static/fake_img.svg"
        />
      `;
    case Tag.CHECK_BOX_OFF:
      return `
        <div class="form-check mtop-${topNumber} mleft-${leftNumber}">
          <input
            class="form-check-input"
            type="checkbox"
            value=""
            id="flexCheckDefault"
          />
        </div>
      `;
    case Tag.CHECK_BOX_ON:
      return `
        <div class="form-check mtop-${topNumber} mleft-${leftNumber}">
          <input
            class="form-check-input"
            type="checkbox"
            value=""
            id="flexCheckDefault"
            checked
          />
        </div>
      `;
    case Tag.RADIO_BUTTON_OFF:
      return `
        <div class="form-check mtop-${topNumber} mleft-${leftNumber}">
          <input
            class="form-check-input"
            type="radio"
            name="flexRadioDefault"
            id="flexRadioDefault1"
          />
        </div>
      `;
    case Tag.RADIO_BUTTON_ON:
      return `
        <div class="form-check mtop-${topNumber} mleft-${leftNumber}">
           <input
            class="form-check-input"
            type="radio"
            name="flexRadioDefault"
            id="flexRadioDefault1"
            checked
           />
        </div>
        `;
    //TODO: Solve problem with height
    case Tag.TEXT_AREA:
      return `
        <div class="width-${colNumber} form-group mtop-${topNumber} mleft-${leftNumber}">
          <textarea
            class="form-control"
            id="exampleFormControlTextarea1"
            rows="3"
            placeholder="Example text area"
          ></textarea>
        </div>
      `;
    case Tag.SLIDER:
      return `
        <div id="myCarousel" class="width-slider-60 carousel slide mtop-${topNumber} slider-center" data-bs-ride="carousel" style="background:#ced4da; height:200px;">
          <ol class="carousel-indicators">
            <li
              data-bs-target="#myCarousel"
              data-bs-slide-to="0"
              class="active"
            ></li>
            <li data-bs-target="#myCarousel" data-bs-slide-to="1"></li>
            <li data-bs-target="#myCarousel" data-bs-slide-to="2"></li>
          </ol>  
          <div class="carousel-inner height-slider-100">
            <div class="carousel-item height-slider-100  active position-relative">
              <p class="height-slider-100 flex-center text-white">1</p>
            </div>
            <div class="carousel-item height-slider-100  position-relative ">
              <p class="height-slider-100 flex-center  text-white">2</p>
            </div>
            <div class="carousel-item height-slider-100 position-relative">
              <p class="height-slider-100 flex-center  text-white">3</p>
            </div>
          </div>
          <a class="carousel-control-prev" href="#myCarousel" data-bs-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#myCarousel" data-bs-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
        </div>

      `;

    case Tag.COMBO_BOX:
      return `
        <div class="width-${colNumber} mtop-${topNumber} mleft-${leftNumber}">
          <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
              Dropdown button
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
              <li><a class="dropdown-item" href="#">Action</a></li>
              <li><a class="dropdown-item" href="#">Another action</a></li>
              <li><a class="dropdown-item" href="#">Something else here</a></li>
            </ul>
          </div>
        </div>
      `;

    case Tag.NUMBER_INPUT:
      return `
        <div class="width-${colNumber} mtop-${topNumber} mleft-${leftNumber}">
          <input
            type="number"
            class="form-control"
            placeholder="Input"
            aria-label="Input"
            aria-describedby="basic-addon1"
          />
        </div>
      `;

    default:
      return '<p>Pruebita</p>';
  }
};

export const skechtHTML = (
  predictions: Prediction[],
  img: probe.ProbeResult,
) => {
  const content = getContent(predictions, img);

  const template = `
  <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Test</title>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
        />
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Styles script from blod storage -->
        <link rel="stylesheet" href="https://wireframes.blob.core.windows.net/static/style.css"/>
      </head>
      <body>
        ${content}
      </body>
    </html>
  `;

  return [template, content];
};
