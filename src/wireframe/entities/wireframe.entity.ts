import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('wireframes')
export class Wireframe {
  constructor();
  constructor(id: string, image_url: string, weight: number, height: number);
  constructor(
    id?: string,
    image_url?: string,
    weight?: number,
    height?: number,
  ) {
    this.id = id;
    this.image_url = image_url;
    this.width = weight;
    this.height = height;
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'imageUrl', type: 'varchar' })
  image_url: string;

  @Column({ type: 'float' })
  width: number;

  @Column({ type: 'float' })
  height: number;
}
