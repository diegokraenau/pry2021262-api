export enum Tag {
  BUTTON = 'BUTTON',
  CARD = 'CARD',
  CHECK_BOX_OFF = 'CHECK_BOX_OFF',
  CHECK_BOX_ON = 'CHECK_BOX_ON',
  CIRCLE_IMAGE = 'CIRCLE_IMAGE',
  COMBO_BOX = 'COMBO_BOX',
  IMAGE = 'IMAGE',
  INPUT = 'INPUT',
  LABEL = 'LABEL',
  NAV_BAR = 'NAV_BAR',
  NUMBER_INPUT = 'NUMBER_INPUT',
  RADIO_BUTTON_OFF = 'RADIO_BUTTON_OFF',
  RADIO_BUTTON_ON = 'RADIO_BUTTON_ON',
  SLIDER = 'SLIDER',
  TEXT_AREA = 'TEXT_AREA',
}

class Box {
  left: number;
  top: number;
  width: number;
  height: number;
}

export class Prediction {
  probability: number;
  tagId: string;
  tagName: Tag;
  boundingBox: Box;
  asignedX = false;
  asignedY = false;
  index: number;
}
