/* eslint-disable prettier/prettier */
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EditViewDto } from 'src/project/dtos/edit-view.dto';
import { Repository } from 'typeorm';
import { View } from './entities';

@Injectable()
export class ViewsService {
  constructor(
    @InjectRepository(View) private readonly viewRepository: Repository<View>,
  ) {}

  async findViewById(id: string) {
    const view = await this.viewRepository.findOne(id);
    if (!view) throw new NotFoundException('Project does not exists');
    return view;
  }

  async createOne(entity: View) {
    return await this.viewRepository.save(entity);
  }

  async deleteViewId(id: string) {
    return await this.viewRepository.delete(id);
  }

  async updateView(id: string, dto: EditViewDto) {
    const view = await this.findViewById(id);
    const viewEdited = Object.assign(view, dto);
    return await this.viewRepository.save(viewEdited);
  }
}
