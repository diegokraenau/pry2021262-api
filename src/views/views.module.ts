import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { View } from './entities';
import { ViewsService } from './views.service';
import { ViewsController } from './views.controller';

@Module({
  imports: [TypeOrmModule.forFeature([View])],
  providers: [ViewsService],
  exports: [ViewsService],
  controllers: [ViewsController],
})
export class ViewsModule {}
