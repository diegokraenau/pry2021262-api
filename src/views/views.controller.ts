/* eslint-disable prettier/prettier */
import { Body, Controller, Delete, Param, Put } from '@nestjs/common';
import { Auth } from 'src/common/decorators';
import { EditViewDto } from 'src/project/dtos/edit-view.dto';
import { ViewsService } from './views.service';

@Controller('views')
export class ViewsController {
  constructor(private readonly viewService: ViewsService) {}

  @Auth()
  @Delete(':id')
  async deleteView(@Param() id: string) {
    const resp = await this.viewService.deleteViewId(id);
    return resp;
  }

  @Auth()
  @Put(':id')
  async updateView(@Param('id') id: string, @Body() dto: EditViewDto) {
    const resp = await this.viewService.updateView(id, dto);
    return resp;
  }
}
