import { Project } from 'src/project/entities';
import { Wireframe } from 'src/wireframe/entities';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

@Entity('views')
export class View {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 20 })
  name: string;

  @Column({ type: 'text' })
  content: string;

  @Column({ type: 'text' })
  template: string;

  @Column({ name: 'created_at', type: 'date' })
  created_date: Date;

  @ManyToOne(() => Project, (project) => project.views, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'project_id' })
  project: Project;

  @OneToOne(() => Wireframe, { onDelete: 'CASCADE' })
  @JoinColumn({ name: 'wireframe_id' })
  wireframe: Wireframe;
}
