import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('history')
export class History {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ name: 'type', type: 'varchar', length: 20 })
  type: string;

  @Column({ name: 'quantity', type: 'int' })
  quantity: number;
}
