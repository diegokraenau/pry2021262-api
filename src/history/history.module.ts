import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { History } from './entities';
import { HistoryService } from './history.service';
import { HistoryController } from './history.controller';
import { UserModule } from '../user/user.module';
import { WireframeModule } from '../wireframe/wireframe.module';
import { ProjectModule } from 'src/project/project.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([History]),
    forwardRef(() => UserModule),
    forwardRef(() => WireframeModule),
    forwardRef(() => ProjectModule),
  ],
  providers: [HistoryService],
  exports: [HistoryService],
  controllers: [HistoryController],
})
export class HistoryModule {}
