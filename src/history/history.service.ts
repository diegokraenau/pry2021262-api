import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { History } from './entities/index';

@Injectable()
export class HistoryService {
  constructor(
    @InjectRepository(History)
    private readonly historyRepository: Repository<History>,
  ) {}

  async countDownloads() {
    return this.historyRepository
      .createQueryBuilder()
      .select('SUM(quantity)', 'total')
      .getRawOne();
  }

  async findOne(data: string) {
    return await this.historyRepository
      .createQueryBuilder()
      .where('type = :data', { data })
      .getOne();
  }

  async addOne(data: string) {
    const item = await this.historyRepository.findOne({
      type: data,
    });

    const res = await this.historyRepository.update(item.id, {
      quantity: item.quantity + 1,
    });

    return res;
  }
}
