import { Controller, Get } from '@nestjs/common';
import { Auth } from 'src/common/decorators';
import { UserService } from 'src/user/user.service';
import { WireframeService } from '../wireframe/wireframe.service';
import { HistoryService } from './history.service';
import { ViewsService } from '../views/views.service';
import { ProjectService } from '../project/project.service';

@Controller('history')
export class HistoryController {
  constructor(
    private readonly userService: UserService,
    private readonly wireframeService: WireframeService,
    private readonly historyService: HistoryService,
    private readonly projectService: ProjectService,
  ) {}

  @Auth()
  @Get('totalData')
  async getTotals() {
    const totalUsers = await this.userService.countUsers();
    const totalWireframes = await this.wireframeService.getWireframes();
    const tD = await this.historyService.countDownloads();
    const totalDownloads = parseInt(tD.total);
    return { totalUsers, totalWireframes, totalDownloads };
  }

  @Auth()
  @Get('frameworkData')
  async getFramework() {
    const React = await this.historyService.findOne('React');
    const Angular = await this.historyService.findOne('Angular');
    const Vue = await this.historyService.findOne('Vue');
    const Html = await this.historyService.findOne('HTML');
    const TotalDownloads = await this.historyService.countDownloads();
    const Downloads = parseInt(TotalDownloads.total) - Html.quantity;

    const totalReact = (React.quantity * 100) / Downloads;
    const totalAngular = (Angular.quantity * 100) / Downloads;
    const totalVue = (Vue.quantity * 100) / Downloads;
    return { totalReact, totalAngular, totalVue };
  }

  @Auth()
  @Get('timeData')
  async getProjectRecords() {
    return await this.projectService.getRecords();
  }
}
