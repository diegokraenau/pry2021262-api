import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards';
import { Auth, User } from 'src/common/decorators';
import { User as UserEntity } from 'src/user/entities';
import { RecoverPasswordDto } from './dtos';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@User() user: UserEntity) {
    const { token } = await this.authService.login(user);
    return {
      token,
    };
  }

  @Auth()
  @Get('private-route-test')
  async privateRouteTest() {
    return 'private-route-test';
  }

  @Post('recover-password')
  async recoverPassword(@Body() dto: RecoverPasswordDto) {
    const sent = await this.authService.sendEmail(dto);
    return sent;
  }

  @Post('reset-folders')
  async resetFolders() {
    const done = await this.authService.resetFolders();
    return done;
  }
}
