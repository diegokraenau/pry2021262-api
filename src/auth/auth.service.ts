import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/user/entities';
import { UserService } from 'src/user/user.service';
import { RecoverPasswordDto } from './dtos';
import { SendGridService } from './send-grid.service';
import * as fs from 'fs-extra';
import { getBasePath } from 'src/project/helpers';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly sendGridService: SendGridService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.findOne({ email });

    //TODO: Fix to base64 password
    if (user && user.password === password) {
      const { password, ...rest } = user;
      return rest;
    }

    return null;
  }

  async login(user: User) {
    const { id } = user;
    return {
      token: this.jwtService.sign({
        id,
        name: user.name,
        lastName: user.lastName,
        role: user.userType,
        imgUrl:
          user?.imgUrl ||
          'https://d3ipks40p8ekbx.cloudfront.net/dam/jcr:3a4e5787-d665-4331-bfa2-76dd0c006c1b/user_icon.png',
      }),
    };
  }

  async sendEmail(dto: RecoverPasswordDto) {
    const user = await this.userService.findOne({
      email: dto.email,
    });
    const mail = {
      to: dto.email,
      subject: 'Recuperar contraseña - Sketch2Web',
      from: 'u201710397@upc.edu.pe',
      text: 'Email para poder recuperar contraseña - Sketch2Web',
      html: `<h1>Buenas tardes, ingresar al <a href="${process.env.RECOVER_PASSWORD_URL}/${user?.id}">link</a> para poder cambiar su contraseña. Saludos.</h1>`,
    };

    return await this.sendGridService.send(mail);
  }

  async resetFolders() {
    //Reset Send Templates Folder
    await fs.rmdir(`${getBasePath()}/ManageTemplates/SendTemplates`, {
      recursive: true,
      force: true,
    });

    //Create Send Templates Folder
    await fs.outputFile(
      `${getBasePath()}/ManageTemplates/SendTemplates/no-empty-folder`,
      'No deleted this file or will not pass to server and github.',
      (err) => {
        if (err) {
          return console.log(err);
        }

        console.log('send-templates-no-empty-folder created');
      },
    );

    //Reset Copy Templates Folder
    await fs.rmdir(`${getBasePath()}/ManageTemplates/CopyTemplates`, {
      recursive: true,
      force: true,
    });

    //Create Copy Templates Folder
    await fs.outputFile(
      `${getBasePath()}/ManageTemplates/CopyTemplates/no-empty-folder`,
      'No deleted this file or will not pass to server and github.',
      (err) => {
        if (err) {
          return console.log(err);
        }

        console.log('copy-templates-no-empty-folder created');
      },
    );

    return true;
  }
}
