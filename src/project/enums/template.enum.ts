export enum Template {
  REACT = 'React',
  ANGULAR = 'Angular',
  VUE = 'Vue',
  ONLY_HTML = 'ONLY_HTML',
}
