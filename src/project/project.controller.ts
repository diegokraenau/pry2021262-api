import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  StreamableFile,
  Response,
  Delete,
  Put,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateProjectDto, EditProject } from './dtos';
import { ProjectService } from './project.service';
import { DownloadProjectDto } from './dtos/download-project.dto';
import { findFile } from './helpers';
import { ExportHTMLDto } from './dtos/export-html.dto';
import { Auth } from 'src/common/decorators';

@ApiTags('Projects')
@Controller('projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Auth()
  @Post()
  async createProject(@Body() dto: CreateProjectDto) {
    const data = await this.projectService.createProject(dto);
    return data;
  }

  @Auth()
  @Get(':id')
  async getProjectById(@Param('id') id: string) {
    const data = await this.projectService.findProjectById(id);
    return data;
  }

  @Auth()
  @Delete(':id')
  async deleteProjectById(@Param('id') id: string) {
    const data = await this.projectService.deleteProjectById(id);
    return data;
  }
  // @Auth()
  // @Get('/get_frameworkds')
  // async getFrameworks() {
  //   const data = getFrameworksCounters();
  //   return data;
  // }

  // @Auth()
  // @Get('/count_total')
  // async getTotalDownloads() {
  //   const data = getDownloads();
  //   return data;
  // }

  // @Auth()
  // @Get('/get_stats')
  // async getAllCounters() {
  //   const data = getDownloads();
  //   return data;
  // }

  @Auth()
  @Post('download')
  async downloadProject(
    @Body() dto: DownloadProjectDto,
    @Response({ passthrough: true }) res,
  ) {
    const folderName = await this.projectService.createZipProjectFolder(dto);
    // addDownload();
    const file = findFile(folderName);
    res.set({
      'Content-Type': 'application/zip',
      'Content-Disposition': `attachment; filename="ProjectDownloaded.zip"`,
    });
    return new StreamableFile(file);
  }

  @Delete('delete-files')
  async deleteContainer() {
    const resp = await this.projectService.deleteFilesBlobStorage();
    return resp;
  }

  @Auth()
  @Post('download-only-html')
  async downloadOnlyHTML(
    @Body() dto: ExportHTMLDto,
    @Response({ passthrough: true }) res,
  ) {
    const folderName = await this.projectService.exportHTML(dto);
    const file = findFile(folderName);
    res.set({
      'Content-Type': 'application/zip',
      'Content-Disposition': `attachment; filename="ProjectDownloaded.zip"`,
    });
    // addHtmlCounter();
    // addDownload();
    return new StreamableFile(file);
  }

  @Auth()
  @Put(':id')
  async updateProjectTheme(@Param('id') id: string, @Body() dto: EditProject) {
    const resp = await this.projectService.updateProject(id, dto);
    return resp;
  }
}
