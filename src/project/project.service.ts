/* eslint-disable prettier/prettier */
import {
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Project } from './entities';
import { CreateProjectDto, DownloadProjectDto } from './dtos';
import {
  compressFolder,
  copyTemplateToFolder,
  addViews,
  addViewsInHTML,
  addDependencies,
  modifyTheme,
} from './helpers';
import { deleteFilesBlobStorage } from 'src/utils';
import { ExportHTMLDto } from './dtos/export-html.dto';
import { Template } from './enums';
import { EditProject } from './dtos';
import { UserService } from 'src/user/user.service';
import { DependencyService } from '../dependency/dependency.service';
import { HistoryService } from 'src/history/history.service';

@Injectable()
export class ProjectService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    private readonly userService: UserService,
    private readonly dependencyService: DependencyService,
    private readonly historyService: HistoryService,
  ) {}

  /*
    Function to find a project by id
  */
  async findProjectById(id: string) {
    const project = await this.projectRepository.findOne(id, {
      relations: ['views'],
    });
    if (!project) throw new NotFoundException('Project does not exists');
    return project;
  }

  /*
    Function to create project
  */
  async createProject(dto: CreateProjectDto) {
    const user = await this.userService.getOne(dto.userId);
    const project = {
      ...dto,
      user,
    };
    const projectUpdated = this.projectRepository.create(project);
    return await this.projectRepository.save(projectUpdated);
  }

  /*
    Function to create the zip folder
  */
  async createZipProjectFolder(dto: DownloadProjectDto) {
    const project = await this.findProjectById(dto.projectId);
    const folderName = project.name + '_' + project.id;
    await copyTemplateToFolder(folderName, dto.templateName);
    await modifyTheme(
      folderName,
      dto.templateName,
      (
        await this.findProjectById(dto.projectId)
      ).theme,
    );
    dto.dependenciesIds?.length > 0 &&
      (await addDependencies(
        folderName,
        dto.templateName,
        await this.getDependenciesById(dto.dependenciesIds),
      ));
    await addViews(project.views, folderName, dto.templateName);
    await compressFolder(folderName);
    //Add register
    await this.historyService.addOne(this.manageTemplateName(dto.templateName));
    return folderName;
  }

  async getRecords() {
    return await this.projectRepository
      .createQueryBuilder()
      .select('Count(id)', 'Cantidad')
      .addSelect('created_at ')
      .groupBy('created_at')
      .orderBy('created_at', 'ASC')
      .getRawMany();
  }

  async deleteProjectById(id: string) {
    return await this.projectRepository.delete(id);
  }

  /*
    Function to find a project by name
  */
  private async findProjectByName(name: string) {
    const project = await this.projectRepository.findOne({ name });
    if (!project) throw new NotFoundException('Project does not exists');
    return project;
  }

  async deleteFilesBlobStorage() {
    const resp = await deleteFilesBlobStorage();
    return resp;
  }

  /*
    Function to export only html
  */
  async exportHTML(dto: ExportHTMLDto) {
    const project = await this.findProjectById(dto.projectId);
    const folderName = project.name + '_' + project.id + '_views';
    await copyTemplateToFolder(folderName, Template.ONLY_HTML);
    await addViewsInHTML(dto.viewsIds, project.views, folderName);
    await compressFolder(folderName);
    return folderName;
  }

  /*
    Function to update project theme
  */
  async updateProject(id: string, dto: EditProject) {
    const project = await this.findProjectById(id);
    const projectEdited = Object.assign(project, dto);
    return await this.projectRepository.save(projectEdited);
  }

  /*
    Function to get projects by user id
  */
  async getProjectsByUserId(id: string) {
    const projects = await this.projectRepository.find({
      where: {
        user: id,
      },
      relations: ['views'],
    });
    return projects;
  }

  /*
    Function to get dependencies array
  */
  async getDependenciesById(dependenciesIds: string[]) {
    const dependencies = [];
    for (const id of dependenciesIds) {
      const dependency = await this.dependencyService.getDependencyById(id);
      dependencies.push(dependency);
    }
    return dependencies;
  }

  private manageTemplateName(templateName: string) {
    switch (templateName) {
      case Template.REACT:
        return 'React';
      case Template.ANGULAR:
        return 'Angular';
      case Template.VUE:
        return 'Vue';
    }
  }
}
