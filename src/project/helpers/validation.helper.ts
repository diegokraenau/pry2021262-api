export const changeComponentName = (originalName: string) => {
  //First letter in capital letter
  originalName = originalName.charAt(0).toUpperCase() + originalName.slice(1);

  //Convert capital letter that letters after special character
  originalName = originalName.replace(new RegExp(/(^|\/|-)(\S)/g), (s) =>
    s.toUpperCase(),
  );

  //Remove all special characters
  originalName = originalName.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, '');

  return originalName;
};
