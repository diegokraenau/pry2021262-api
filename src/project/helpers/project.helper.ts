import { Template } from '../enums';
import * as compressing from 'compressing';
import * as fs from 'fs-extra';
import { join } from 'path';
import { createReadStream } from 'fs';
import { View } from '../../views/entities';
import { changeComponentName } from '.';
import { Dependency } from 'src/dependency/entities/dependency.entity';

/*
    Function to get the base path
*/
export const getBasePath = () => {
  return process.cwd();
};

/*
    Function to get the path of template selected
*/
export const getTemplatePathSelected = (template: string) => {
  const templateCompleteName = template + 'Template';
  switch (template) {
    case Template.REACT:
      return `${getBasePath()}/ManageTemplates/Templates/${templateCompleteName}`;
    case Template.ANGULAR:
      return `${getBasePath()}/ManageTemplates/Templates/${templateCompleteName}`;
    case Template.VUE:
      return `${getBasePath()}/ManageTemplates/Templates/${templateCompleteName}`;
    case Template.ONLY_HTML:
      return `${getBasePath()}/ManageTemplates/Templates/OnlyHTML`;
    default:
      break;
  }
};

/*
    Function to create a folder in CopyTemplates with the template selected
*/
export const copyTemplateToFolder = async (
  folderName: string,
  templateName: string,
) => {
  const projectDir = `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}`;
  if (!(await fs.existsSync(projectDir))) {
    await fs.mkdir(projectDir);
  } else {
    await fs.rmdir(projectDir, { recursive: true, force: true });
    await fs.mkdir(projectDir);
  }

  const templateDir = getTemplatePathSelected(templateName);
  await fs.copy(templateDir, projectDir, { overwrite: true });
};

/*
    Function to compress the project folder to zip
*/
export const compressFolder = async (projectFolderName: string) => {
  await compressing.zip
    .compressDir(
      `${getBasePath()}/ManageTemplates/CopyTemplates/${projectFolderName}`,
      `${getBasePath()}/ManageTemplates/SendTemplates/${projectFolderName}.zip`,
    )
    .then(() => {
      console.log(`Folder ${projectFolderName} Compressed`);
    })
    .catch((err) => {
      console.log(err);
    });
};

/*
    Function to get the path of folder compressed
*/
export const findFile = (folderName: string) => {
  return createReadStream(
    join(process.cwd(), `/ManageTemplates/SendTemplates/${folderName}.zip`),
  );
};

/*
    Function to generate views inside folder
*/
export const addViews = async (
  views: View[],
  folderName: string,
  templateName: string,
) => {
  for (const view of views) {
    await addOneView(view, folderName, templateName);
  }

  //Adding view to module if its angular template
  if (templateName == Template.ANGULAR) {
    await addViewsToModule(views, folderName);
  }

  //Adding view to export default if its vue template
  if (templateName == Template.VUE) {
    await addViewsToExport(views, folderName);
  }
};

/*
    Function to generate views with only HTML inside folder
*/
export const addViewsInHTML = async (
  viewsIds: string[],
  views: View[],
  folderName: string,
) => {
  for await (const id of viewsIds) {
    const view = views.find((x) => x.id === id);
    addViewHTML(view, folderName);
  }

  await compressFolder(folderName);
};

/*
  Adding dependencies on package.json
*/
export const addDependencies = async (
  folderName: string,
  templateName: string,
  dependencies: Dependency[],
) => {
  console.log({ templateName });
  switch (templateName) {
    case Template.REACT:
      await addDependenciesReact(folderName, dependencies);
      break;
    case Template.ANGULAR:
      await addDependenciesAngular(folderName, dependencies);
      break;
    case Template.VUE:
      await addDependenciesVue(folderName, dependencies);
      break;
    default:
      break;
  }
};

/*
  Adding dependencies on package.json
*/
export const modifyTheme = async (
  folderName: string,
  templateName: string,
  theme: string,
) => {
  switch (templateName) {
    case Template.REACT:
      await addThemeReact(folderName, theme);
      break;
    case Template.ANGULAR:
      await addThemeAngular(folderName, theme);
    case Template.VUE:
      await addThemeVue(folderName, theme);

    default:
      break;
  }
};

// Private methods

/*
    Function to create one view depend on framework selected
*/
const addOneView = async (
  view: View,
  folderName: string,
  templateName: string,
) => {
  switch (templateName) {
    case Template.REACT:
      await createReactView(view, folderName);
      break;
    case Template.ANGULAR:
      await createAngularView(view, folderName);
      break;
    case Template.VUE:
      await createVueView(view, folderName);
      break;

    default:
      break;
  }
};

/*
    Function to create react view
*/
const createReactView = async (view: View, folderName: string) => {
  let viewDecoded = Buffer.from(view.content, 'base64').toString('ascii');
  viewDecoded = viewDecoded.replace(/class\b/g, 'className');

  const viewTemplate = `
  import React from 'react'

  const ${changeComponentName(view.name)} = () => {
      return (
        <>
          ${viewDecoded}
        </>
      )
  }

  export default ${changeComponentName(view.name)}
  `;

  await fs.outputFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/views/${
      view.name
    }.jsx`,
    viewTemplate,
    function (err) {
      if (err) throw err;
      console.log('View created!');
    },
  );
};

/*
    Function to create vue view
*/
const createVueView = async (view: View, folderName: string) => {
  const viewDecoded = Buffer.from(view.content, 'base64').toString('ascii');

  const viewTemplate = `
    <template>
      <div>
        ${viewDecoded}
      </div>
    </template>

    <script>
    export default {
      name: '${changeComponentName(view.name)}',
      props: {},
    };
    </script>

    <!-- Add "scoped" attribute to limit CSS to this component only -->
    <style lang="scss">
    </style>
  `;

  await fs.outputFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/components/${changeComponentName(
      view.name,
    )}.vue`,
    viewTemplate,
    function (err) {
      if (err) throw err;
      console.log('View created!');
    },
  );
};

/*
    Function to create angular view
*/

const createAngularView = async (view: View, folderName: string) => {
  const viewDecoded = Buffer.from(view.content, 'base64').toString('ascii');
  const arrFiles = [];
  // viewDecoded = viewDecoded.replace(/class\b/g, 'className');

  const tsFile = {
    type: 'ts',
    content: `
    import { Component } from '@angular/core';
  
    @Component({
      selector: 'app-${view.name}',
      templateUrl: './${view.name}.component.html',
      styleUrls: ['./${view.name}.component.scss']
    })
    export class ${changeComponentName(view.name)} {
      
    }
    `,
  };

  const htmlFile = {
    type: 'html',
    content: viewDecoded,
  };

  const scssFile = {
    type: 'scss',
    content: `
      .contenedor-prueba {}
    `,
  };

  arrFiles.push(tsFile);
  arrFiles.push(htmlFile);
  arrFiles.push(scssFile);

  for (const file of arrFiles) {
    await fs.outputFile(
      `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/app/views/${
        view.name
      }/${view.name}.component.${file.type}`,
      file.content,
      function (err) {
        if (err) throw err;
        console.log(`.${file.type} created - ${view.name}`);
      },
    );
  }
};

/*
    Function to add views to module angular
*/

const addViewsToModule = async (views: View[], folderName: string) => {
  const fileUpdated = `
  import { NgModule } from '@angular/core';
  import { BrowserModule } from '@angular/platform-browser';
  
  import { AppRoutingModule } from './app-routing.module';
  import { AppComponent } from './app.component';
  ${views
    .map(
      (x) =>
        `import {${changeComponentName(x.name)}} from './views/${x.name}/${
          x.name
        }.component';`,
    )
    .join('\n')}
  
  @NgModule({
    declarations: [
      AppComponent,
      ${views.map((x) => changeComponentName(x.name)).join(',')}
    ],
    imports: [
      BrowserModule,
      AppRoutingModule
    ],
    providers: [],
    bootstrap: [AppComponent]
  })
  export class AppModule { }
  `;

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/app/app.module.ts`,
    fileUpdated,
    (err) => {
      if (err) console.log(err);
      else {
        console.log('app.module.ts edited\n');
      }
    },
  );
};

/*
    Function to add views to module vue
*/

const addViewsToExport = async (views: View[], folderName: string) => {
  const fileUpdated = `
  <template></template>

  <script>
  ${views
    .map(
      (x) =>
        `import ${changeComponentName(
          x.name,
        )} from './components/${changeComponentName(x.name)}.vue';`,
    )
    .join('\n')}

  export default {
    name: "App",
    components: {
      ${views.map((x) => changeComponentName(x.name)).join(',')}
    },
  };
  </script>

  <style>
  </style>
  `;

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/App.vue`,
    fileUpdated,
    (err) => {
      if (err) console.log(err);
      else {
        console.log('app.vue edited\n');
      }
    },
  );
};

/*
  Function to add HTML view inside folder
*/

const addViewHTML = async (view: View, folderName: string) => {
  const viewDecoded = Buffer.from(view.content, 'base64').toString('ascii');
  const viewTemplateWithLocalStyles = `
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Test</title>
      <link
        rel="stylesheet"
        href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css"
      />
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"></script>
      <!-- Styles script from blod storage -->
      <link
        rel="stylesheet"
        href="./styles.css"
      />
    </head>
    <body>
      ${viewDecoded}
    </body>
  </html>
  `;

  await fs.outputFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/${
      view.name
    }.html`,
    viewTemplateWithLocalStyles,
    function (err) {
      if (err) throw err;
      console.log('View-HTML created!');
    },
  );
};

/*
  Function to add dependencies to react
*/
const addDependenciesReact = async (
  folderName: string,
  dependencies: Dependency[],
) => {
  const packageJsonUpdated = {
    name: 'project',
    version: '0.1.0',
    private: true,
    dependencies: {
      '@testing-library/jest-dom': '^5.11.4',
      '@testing-library/react': '^11.1.0',
      '@testing-library/user-event': '^12.1.10',
      react: '^17.0.2',
      'react-dom': '^17.0.2',
      'react-scripts': '4.0.3',
      'web-vitals': '^1.0.1',
      bootstrap: '^5.1.3',
      'node-sass': '^6.0.1',
    },
    scripts: {
      start: 'react-scripts start',
      build: 'react-scripts build',
      test: 'react-scripts test',
      eject: 'react-scripts eject',
    },
    eslintConfig: {
      extends: ['react-app', 'react-app/jest'],
    },
    browserslist: {
      production: ['>0.2%', 'not dead', 'not op_mini all'],
      development: [
        'last 1 chrome version',
        'last 1 firefox version',
        'last 1 safari version',
      ],
    },
  };

  dependencies.forEach((x) => {
    packageJsonUpdated.dependencies[x.name] = x.version;
  });

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/package.json`,
    JSON.stringify(packageJsonUpdated),
    (err) => {
      if (err) console.log(err);
      else {
        console.log(`package.json edited - ${folderName}\n`);
      }
    },
  );
};

/*
  Function to add dependencies to react
*/
const addDependenciesVue = async (
  folderName: string,
  dependencies: Dependency[],
) => {
  const packageJsonUpdated = {
    name: 'template',
    version: '0.1.0',
    private: true,
    scripts: {
      serve: 'vue-cli-service serve',
      build: 'vue-cli-service build',
      lint: 'vue-cli-service lint',
    },
    dependencies: {
      '@types/bootstrap': '^5.1.10',
      '@types/jquery': '^3.5.14',
      '@types/popper.js': '^1.11.0',
      bootstrap: '^5.1.3',
      'core-js': '^3.8.3',
      jquery: '^3.6.0',
      'popper.js': '^1.16.1',
      vue: '^3.2.13',
    },
    devDependencies: {
      '@babel/core': '^7.12.16',
      '@babel/eslint-parser': '^7.12.16',
      '@types/node-sass': '^4.11.2',
      '@types/sass-loader': '^8.0.3',
      '@vue/cli-plugin-babel': '~5.0.0',
      '@vue/cli-plugin-eslint': '~5.0.0',
      '@vue/cli-service': '~5.0.0',
      eslint: '^7.32.0',
      'eslint-plugin-vue': '^8.0.3',
      'node-sass': '^7.0.1',
      'sass-loader': '^12.6.0',
    },
    eslintConfig: {
      root: true,
      env: {
        node: true,
      },
      extends: ['plugin:vue/vue3-essential', 'eslint:recommended'],
      parserOptions: {
        parser: '@babel/eslint-parser',
      },
      rules: {},
    },
    browserslist: ['> 1%', 'last 2 versions', 'not dead', 'not ie 11'],
  };

  console.log({ dependencies });

  dependencies.forEach((x) => {
    packageJsonUpdated.dependencies[x.name] = x.version;
  });

  console.log({ packageJsonUpdated });

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/package.json`,
    JSON.stringify(packageJsonUpdated),
    (err) => {
      if (err) console.log(err);
      else {
        console.log(`package.json edited - ${folderName}\n`);
      }
    },
  );
};

/*
  Function to add dependencies to react
*/
const addDependenciesAngular = async (
  folderName: string,
  dependencies: Dependency[],
) => {
  const packageJsonUpdated = {
    name: 'project',
    version: '0.0.0',
    scripts: {
      ng: 'ng',
      start: 'ng serve',
      build: 'ng build',
      watch: 'ng build --watch --configuration development',
      test: 'ng test',
    },
    private: true,
    dependencies: {
      '@angular/animations': '~13.1.0',
      '@angular/common': '~13.1.0',
      '@angular/compiler': '~13.1.0',
      '@angular/core': '~13.1.0',
      '@angular/forms': '~13.1.0',
      '@angular/platform-browser': '~13.1.0',
      '@angular/platform-browser-dynamic': '~13.1.0',
      '@angular/router': '~13.1.0',
      '@popperjs/core': '^2.11.2',
      '@types/bootstrap': '^5.1.9',
      '@types/jquery': '^3.5.13',
      bootstrap: '^5.1.3',
      jquery: '^3.6.0',
      rxjs: '~7.4.0',
      tslib: '^2.3.0',
      'zone.js': '~0.11.4',
    },
    devDependencies: {
      '@angular-devkit/build-angular': '~13.1.2',
      '@angular/cli': '~13.1.2',
      '@angular/compiler-cli': '~13.1.0',
      '@types/jasmine': '~3.10.0',
      '@types/node': '^12.11.1',
      'jasmine-core': '~3.10.0',
      karma: '~6.3.0',
      'karma-chrome-launcher': '~3.1.0',
      'karma-coverage': '~2.1.0',
      'karma-jasmine': '~4.0.0',
      'karma-jasmine-html-reporter': '~1.7.0',
      typescript: '~4.5.2',
    },
  };

  dependencies.forEach((x) => {
    packageJsonUpdated.dependencies[x.name] = x.version;
  });

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/package.json`,
    JSON.stringify(packageJsonUpdated),
    (err) => {
      if (err) console.log(err);
      else {
        console.log(`package.json edited - ${folderName}\n`);
      }
    },
  );
};

/*
  Function to add react theme
*/
const addThemeReact = async (folderName: string, theme: string) => {
  const packageJsonUpdated = `
   
    .fill {
      width: 100vw;
      height: 100vh;
    }
    
    .cover-img {
      height: 60%;
      object-fit: cover;
    }
    
    .circle {
      border-radius: 50%;
    }
    
    @for $i from 1 through 100 {
      .mtop-#{$i} {
        position: absolute !important;
        top: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 100 {
      .mleft-#{$i} {
        position: absolute !important;
        left: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .height-#{$i} {
        height: #{$i * 16,67%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .width-#{$i} {
        width: #{$i * 16,67%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .circle-#{$i} {
        width: #{$i * 130px} !important;
        height: #{$i * 130px} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .square-#{$i} {
        width: #{$i * 200px} !important;
        height: #{$i * 200px} !important;
      }
    }
    
    .image-fixed {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    
    @for $i from 1 through 100  {
      .height-slider-#{$i} {
        height: #{$i * 1%} ;
      }
    }
    
    @for $i from 1 through 100 {
      .width-slider-#{$i} {
        width: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 1000  {
      .left-slider-#{$i}px {
        position: absolute;
        left: #{$i * 1px};
      }
    }
    
    .flex-center {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    
    .slider-center {
      left: 0;
      right: 0;
      margin: auto;
    }
    
    $primary: ${theme};
    $theme-colors: (
      primary: $primary,
    );
    
    @import '~bootstrap/scss/bootstrap.scss';
    `;

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/index.scss`,
    packageJsonUpdated,
    (err) => {
      if (err) console.log(err);
      else {
        console.log(`index.scss edited - ${folderName}\n`);
      }
    },
  );
};

/*
  Function to add vue theme
*/
const addThemeVue = async (folderName: string, theme: string) => {
  const packageJsonUpdated = `
   
    .fill {
      width: 100vw;
      height: 100vh;
    }
    
    .cover-img {
      height: 60%;
      object-fit: cover;
    }
    
    .circle {
      border-radius: 50%;
    }
    
    @for $i from 1 through 100 {
      .mtop-#{$i} {
        position: absolute !important;
        top: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 100 {
      .mleft-#{$i} {
        position: absolute !important;
        left: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .height-#{$i} {
        height: #{$i * 16,67%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .width-#{$i} {
        width: #{$i * 16,67%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .circle-#{$i} {
        width: #{$i * 130px} !important;
        height: #{$i * 130px} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .square-#{$i} {
        width: #{$i * 200px} !important;
        height: #{$i * 200px} !important;
      }
    }
    
    .image-fixed {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    
    @for $i from 1 through 100  {
      .height-slider-#{$i} {
        height: #{$i * 1%} ;
      }
    }
    
    @for $i from 1 through 100 {
      .width-slider-#{$i} {
        width: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 1000  {
      .left-slider-#{$i}px {
        position: absolute;
        left: #{$i * 1px};
      }
    }
    
    .flex-center {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    
    .slider-center {
      left: 0;
      right: 0;
      margin: auto;
    }

    $primary: ${theme};
    $theme-colors: (
      primary: $primary,
    );
    
    @import '~bootstrap/scss/bootstrap.scss';
    `;

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/index.scss`,
    packageJsonUpdated,
    (err) => {
      if (err) console.log(err);
      else {
        console.log(`index.scss edited - ${folderName}\n`);
      }
    },
  );
};

/*
  Function to add angular theme
*/
const addThemeAngular = async (folderName: string, theme: string) => {
  const packageJsonUpdated = `
    
    .fill {
      width: 100vw;
      height: 100vh;
    }
    
    .cover-img {
      height: 60%;
      object-fit: cover;
    }
    
    .circle {
      border-radius: 50%;
    }
    
    @for $i from 1 through 100 {
      .mtop-#{$i} {
        position: absolute !important;
        top: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 100 {
      .mleft-#{$i} {
        position: absolute !important;
        left: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .height-#{$i} {
        height: #{$i * 16,67%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .width-#{$i} {
        width: #{$i * 16,67%} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .circle-#{$i} {
        width: #{$i * 130px} !important;
        height: #{$i * 130px} !important;
      }
    }
    
    @for $i from 1 through 6 {
      .square-#{$i} {
        width: #{$i * 200px} !important;
        height: #{$i * 200px} !important;
      }
    }
    
    .image-fixed {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
    
    @for $i from 1 through 100  {
      .height-slider-#{$i} {
        height: #{$i * 1%} ;
      }
    }
    
    @for $i from 1 through 100 {
      .width-slider-#{$i} {
        width: #{$i * 1%} !important;
      }
    }
    
    @for $i from 1 through 1000  {
      .left-slider-#{$i}px {
        position: absolute;
        left: #{$i * 1px};
      }
    }
    
    .flex-center {
      display: flex;
      justify-content: center;
      align-items: center;
    }
    
    .slider-center {
      left: 0;
      right: 0;
      margin: auto;
    }
    
    
    $primary: ${theme};
    $theme-colors: (
      primary: $primary,
    );
    
    @import '~bootstrap/scss/bootstrap.scss';
    `;

  await fs.writeFile(
    `${getBasePath()}/ManageTemplates/CopyTemplates/${folderName}/src/styles.scss`,
    packageJsonUpdated,
    (err) => {
      if (err) console.log(err);
      else {
        console.log(`index.scss edited - ${folderName}\n`);
      }
    },
  );
};
