import { User } from 'src/user/entities';
import { View } from 'src/views/entities';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('projects')
export class Project {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'varchar', length: 20 })
  name: string;

  @Column({ name: 'created_at', type: 'date' })
  created_date: Date;

  @Column({ name: 'theme', type: 'varchar', length: 10, nullable: true })
  theme: string;

  @OneToMany(() => View, (view) => view.project)
  views: View[];

  @ManyToOne(() => User, (user) => user.projects, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id' })
  user: User;
}
