import { forwardRef, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DependencyModule } from 'src/dependency/dependency.module';
import { HistoryModule } from 'src/history/history.module';
import { UserModule } from 'src/user/user.module';
import { Project } from './entities';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Project]),
    forwardRef(() => UserModule),
    forwardRef(() => DependencyModule),
    forwardRef(() => HistoryModule),
    // HistoryModule
  ],
  controllers: [ProjectController],
  providers: [ProjectService],
  exports: [ProjectService],
})
export class ProjectModule {}
