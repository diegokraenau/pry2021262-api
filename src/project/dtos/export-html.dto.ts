import { IsNotEmpty, IsString } from 'class-validator';

export class ExportHTMLDto {
  @IsString()
  @IsNotEmpty()
  projectId: string;

  @IsNotEmpty()
  viewsIds: string[];
}
