import { IsNotEmpty, IsString } from 'class-validator';

export class DownloadProjectDto {
  @IsString()
  @IsNotEmpty()
  projectId: string;

  @IsString()
  @IsNotEmpty()
  templateName: string;

  @IsNotEmpty()
  dependenciesIds: string[];
}
