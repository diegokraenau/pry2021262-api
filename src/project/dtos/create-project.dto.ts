import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class CreateProjectDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  created_date: Date;

  @IsString()
  @IsOptional()
  theme: string;

  @IsString()
  @IsNotEmpty()
  userId: string;
}
