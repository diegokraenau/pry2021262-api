import { IsNotEmpty, IsString } from 'class-validator';

export class EditViewDto {
  @IsString()
  @IsNotEmpty()
  name: string;
}
