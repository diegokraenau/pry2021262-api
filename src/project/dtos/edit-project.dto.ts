import { OmitType, PartialType } from '@nestjs/swagger';
import { CreateProjectDto } from './create-project.dto';

export class EditProject extends PartialType(
  OmitType(CreateProjectDto, ['created_date', 'userId'] as const),
) {}
