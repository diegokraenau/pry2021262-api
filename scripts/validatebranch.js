/**
 * Script para añadir el nombre de la rama al inicio del mensaje al hacer un commit
 *
 */
 var fs = require('fs');
 const { spawnSync} = require('child_process');

 // Obtiene el nombre de la rama activa
 const child = spawnSync('git', ['rev-parse', '--abbrev-ref', 'HEAD']);
 
 if (child.error) {
 
   console.log('Error obteniendo rama activa', child.error);
   process.exit(1);
 }
 
 // Nombre de la rama
 const branchName = child.stdout.toString('utf8').trim();
 
 // Regex para comparar el nombre de la rama
 const BRANCH_FEATURE_NAME = /^feature/;

 
 if (!BRANCH_FEATURE_NAME.test(branchName)) {
 
   console.log('La rama activa no cumple con las normas de nomenclatura del proyecto', branchName);
   process.exit(1);
 }
 