--USERS
INSERT INTO users (id,name,last_name,birthday,created_at,email,password ) VALUES (UUID(),'Diego','Kraenau','2000-06-21','2021-10-16','diegokraenau@gmail.com','diego2009');
INSERT INTO users (id,name,last_name,birthday,created_at,email,password, user_type) VALUES (UUID(),'Uusario Admin','Usuario Admin','2000-06-21','2021-10-16','admin@gmail.com','admin2009', 'ADMINISTRATOR');

--FRAMEWORKS
INSERT INTO frameworks(id, framework) VALUES (1, 'ANGULAR');
INSERT INTO frameworks(id, framework) VALUES (2, 'REACT');
INSERT INTO frameworks(id, framework) VALUES (3, 'VUE');

--DEPENDENCIES
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'react-router-dom', '^5.2.0', 2);
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'styled-components', '^5.3.3', 2);
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'react-hook-form', '^7.9.0', 2);
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'react-query', '^3.8.3', 2);
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'yup', '^0.32.11', 2);

INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'@swimlane/ngx-charts', '^20.1.0', 1);
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'@ngx-translate/core', '^14.0.0', 1);
INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'@ngrx/store', '^12.4.0', 1);

INSERT INTO dependencies(id, name, version, framework_id) VALUES (UUID() ,'axios', '^0.21.1', 3);

--HISTORY
INSERT INTO history(id, type, quantity) VALUES (UUID() ,'HTML', 0);
INSERT INTO history(id, type, quantity) VALUES (UUID() ,'React', 0);
INSERT INTO history(id, type, quantity) VALUES (UUID() ,'Angular', 0);
INSERT INTO history(id, type, quantity) VALUES (UUID() ,'Vue', 0);

